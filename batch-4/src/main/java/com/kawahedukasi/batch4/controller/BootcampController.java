package com.kawahedukasi.batch4.controller;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/bootcamp")
@ApplicationScoped
public class BootcampController {
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String post(){
        return "GET : Ini merupakan output GET HTTP VERBS";
    }

    @POST
    @Produces(MediaType.TEXT_PLAIN)
    public String get(){
        return "POST : Ini merupakan output POST HTTP VERBS";
    }

    @PUT
    @Produces(MediaType.TEXT_PLAIN)
    public String put(){
        return "PUT : Ini merupakan output PUT HTTP VERBS";
    }

    @DELETE
    @Produces(MediaType.TEXT_PLAIN)
    public String delete(){
        return "DELETE : Ini merupakan output DELETE HTTP VERBS";
    }
}
